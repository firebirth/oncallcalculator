﻿open System
open Calculator
open System.IO

type Span = Span of TimeSpan with
    static member (+) (d: DateTime, Span wrapper) = d + wrapper
    static member Zero = Span(TimeSpan(0L))

let getShiftsFor startHour endHour (startDate: DateTime) (endDate: DateTime) =
    let inc = TimeSpan.FromDays(1.0)
    if (startHour < endHour) then
        let starts = [ startDate.Date.AddHours(startHour) .. Span(inc) .. endDate.Date.AddHours(startHour) ]
        let ends = [ startDate.Date.AddHours(endHour) .. Span(inc) .. endDate.Date.AddHours(endHour) ]
        List.map2 (fun s e -> { Start = s; End = e }) starts ends
    else
        let starts = [ startDate.Date.AddHours(startHour) .. Span(inc) .. endDate.Date.AddDays(-1.).AddHours(startHour) ]
        let ends = [ startDate.Date.AddDays(1.).AddHours(endHour) .. Span(inc) .. endDate.Date.AddHours(endHour) ]
        let main = List.map2 (fun s e -> { Start = s; End = e }) starts ends
        let edges =
            [ { Start = startDate.Date; End = startDate.Date.AddHours(endHour) };
              { Start = endDate.Date.AddHours(startHour); End = endDate.Date.AddDays(1.) } ]
        main @ edges

let printShifts shifts name =
    let weekFormat (tw:TextWriter) ((weekStart, weekEnd): DateTime * DateTime) = tw.Write("{0:dddd, yyyy-MM-dd} - {1:dddd, yyyy-MM-dd}", weekStart, weekEnd)
    let groupedShifts = shifts |> Calculator.merge |> Calculator.groupByWeek

    printfn name

    for (weekStart, weekShifts) in groupedShifts do
        printfn "--------------------------------------------------"
        let week = (weekStart, weekStart.AddDays(6.)) in
            printfn "Week: %a" weekFormat week
        weekShifts |> List.sumBy (fun s -> (s.End - s.Start).TotalDays) |> printfn "On-call days: %g"
        List.iter (printfn "%A") weekShifts

    printfn "++++++++++++++++++++++++++++++++++++++++++++++++++"

let moCalls =
    let shiftGenerator =
        let (startTime, endTime) = (9., 21.) in getShiftsFor startTime endTime
    let shifts = shiftGenerator (DateTime(2019, 9, 1)) (DateTime(2019, 9, 2)) @ shiftGenerator (DateTime(2019, 9, 8)) (DateTime(2019, 10, 5))
    let overrides = []
    shifts @ overrides

let vcCalls =
    let shiftGenerator =
        let (startTime, endTime) = (21., 9.) in getShiftsFor startTime endTime
    let shifts = shiftGenerator (DateTime(2019, 9, 1)) (DateTime(2019, 10, 5))
    let overrides = [ { Start = DateTime(2019, 9, 2, 21, 0, 0); End = DateTime(2019, 9, 8, 9, 0, 0) } ]
    shifts @ overrides

[<EntryPoint>]
let main _ =
    printShifts moCalls "Maksym Olkhovych"
    printShifts vcCalls "Volodymyr Chechel"
    0 // return an integer exit code
