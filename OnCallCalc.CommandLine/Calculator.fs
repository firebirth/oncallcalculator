﻿module Calculator

open System

[<StructuredFormatDisplay("Start: {Start}; End: {End}")>]
type Interval<'a> = { Start:'a; End:'a }

let merge intervals =
    let rec mergeInternal = 
        // assumption is that intervals are sorted by the Start, so x.Start <= y.Start
        function
        | x :: tail when x.Start = x.End -> mergeInternal tail // x is empty
        | x :: y :: tail when x.End >= y.End -> x :: tail |> mergeInternal // x contains y
        | x :: y :: tail when y.Start <= x.End -> { Start = x.Start; End = y.End } :: tail |> mergeInternal // x and y intersect
        | x :: tail -> x :: mergeInternal tail // x and y do not intersect
        | [] -> []
    intervals |> List.sortBy (fun i -> i.Start) |> mergeInternal

let groupByWeek =
    let startOfWeek startOfWeek (interval: Interval<DateTime>) =
        let diff = (7 + (int)(interval.Start.DayOfWeek - startOfWeek)) % 7
        let firstDay = interval.Start.AddDays (-(float)diff) in
            firstDay.Date
    
    List.groupBy (startOfWeek DayOfWeek.Monday)